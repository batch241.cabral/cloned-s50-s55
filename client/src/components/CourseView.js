import { useContext, useEffect, useState } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import userContext from "../UserContext";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

export default function CourseView() {
    const navigate = useNavigate();
    const { user } = useContext(userContext);
    const { courseId } = useParams();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
            .then((res) => res.json())
            .then((data) => {
                console.log(data.course[0]);
                setName(data.course[0].name);
                setDescription(data.course[0].description);
                setPrice(data.course[0].price);
            });
    }, [courseId]);

    function enroll(id) {
        fetch(`${process.env.REACT_APP_API_URL}/users/${courseId}/enroll`, {
            method: "POST",
            headers: {
                "Content-type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            body: JSON.stringify({
                userID: id._id,
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                if (data) {
                    Swal.fire({
                        title: "Successfully enrolled",
                        icon: "success",
                        text: "You have successfully enrolled for this course.",
                    });
                    navigate("/courses");
                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again.",
                    });
                }
            });
    }
    return (
        <Container>
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className='text-center'>
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            <Card.Subtitle>Class Schedule:</Card.Subtitle>
                            <Card.Text>8:00 AM - 5:00 PM</Card.Text>

                            {user.id !== null ? (
                                <Button
                                    variant='primary'
                                    onClick={() => enroll(courseId)}
                                >
                                    Enroll
                                </Button>
                            ) : (
                                <Button className='btn btn-danger' as={Link} to='/login'>
                                    Log in to Enroll
                                </Button>
                            )}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}
