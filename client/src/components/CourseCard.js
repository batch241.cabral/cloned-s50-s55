import { useState, useEffect } from "react";

// S50 ACTIVITY
import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function CourseCard({ course }) {
    // const {name, description, price} = course;
    // S51 ACTIVITY
    const { name, description, price, _id } = course;

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>

                <Button as={Link} to={`/courses/${_id}`} className='bg-primary'>
                    Details
                </Button>
            </Card.Body>
        </Card>
    );
}
