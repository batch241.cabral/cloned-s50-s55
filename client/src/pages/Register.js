import { useState, useEffect } from "react";

import { Form, Button } from "react-bootstrap";

import { useNavigate } from "react-router-dom";
import userContext from "../UserContext";
import { useContext } from "react";
import Swal from "sweetalert2";

export default function Register() {
    const navigate = useNavigate();

    const { user } = useContext(userContext);

    useEffect(() => {
        if (user.id) {
            navigate("/courses");
        }
    });

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    // State to determine whether submit button will be enabled or not
    const [isActive, setIsActive] = useState(false);

    // Function to simulate user registration
    function registerUser(e) {
        // Prevents page from reloading
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
                "Content-type": "application/json",
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                mobileNo: mobileNo,
                password: password1,
                email: email,
            }),
        }).then((res) => {
            console.log(res);
            if (res.ok) {
                Swal.fire({
                    title: "Successfully enrolled",
                    icon: "success",
                    text: "You have successfully enrolled for this course.",
                });
                navigate("/login");
            } else if (res.status === 401) {
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "This email address is already registered. Please use a different email address.",
                });
                navigate("/login");
            } else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again.",
                });
                navigate("/login");
            }
        });
    }
    useEffect(() => {
        const isValid = /^\d{11}$/.test(mobileNo);
        document
            .getElementById("mobileNo")
            .setCustomValidity(
                isValid ? "" : "Please enter a valid 11-digit mobile number"
            );
    }, [mobileNo]);

    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if (
            email !== "" &&
            password1 !== "" &&
            password2 !== "" &&
            firstName !== "" &&
            lastName !== "" &&
            mobileNo !== "" &&
            password1 === password2
        ) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
        // Dependencies
        // No dependencies - effect function will run every time component renders
        // With dependency (empty array) - effect function will only run (one time) when the component renders
        // With dependencies - effect function will run anytime one of the values in the array of dependencies changes
    }, [email, password1, password2, firstName, lastName, mobileNo]);

    return (
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId='firstName'>
                <Form.Label>First Name</Form.Label>
                <Form.Control
                    type='text'
                    placeholder='firstName'
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId='lastName'>
                <Form.Label>First Name</Form.Label>
                <Form.Control
                    type='text'
                    placeholder='lastName'
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId='userEmail'>
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    type='email'
                    placeholder='Enter email'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
                <Form.Text className='text-muted'>
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId='mobileNo'>
                <Form.Label>First Name</Form.Label>
                <Form.Control
                    type='text'
                    placeholder='mobileNo'
                    value={mobileNo}
                    onChange={(e) => setMobileNo(e.target.value)}
                    required
                />
                <Form.Control.Feedback type='invalid'>
                    Please enter a valid 11-digit mobile number
                </Form.Control.Feedback>
            </Form.Group>

            <Form.Group controlId='password1'>
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type='password'
                    placeholder='Password'
                    value={password1}
                    onChange={(e) => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId='password2'>
                <Form.Label>Verify Password</Form.Label>
                <Form.Control
                    type='password'
                    placeholder='Verify Password'
                    value={password2}
                    onChange={(e) => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>
            {/*conditionally render submit button based on "isActive" state*/}
            {isActive ? (
                <Button variant='primary' type='submit' id='submitBtn'>
                    Submit
                </Button>
            ) : (
                <Button disabled variant='primary' type='submit' id='submitBtn'>
                    Submit
                </Button>
            )}
        </Form>
    );
}
